/*
 * main.cpp
 *
 *  Created on: Mar 18, 2016
 *      Author: kina
 */

#include "time.h"
#include "stdlib.h"

//#include "math.h"
#include "Colony.h"


int main (){


// parameters for our optimization algorithm
int ant_number = 1;
int max_iter = 1;
float alpha = 0.9;
float beta = 0.8;
float evaporateCoeff = 0.05;
int ProblemSize = 100;
unsigned randomCity = 100; // percentage of probability for an ant to take a random city

// fill in the initial pheromone trails to be zero.
float PheromoneDeposits[ProblemSize*ProblemSize];
for (int i = 0; i<ProblemSize*ProblemSize; i++)
	PheromoneDeposits [i] = 0;

srand(time(NULL));
Colony* myColony = new Colony(PheromoneDeposits, ProblemSize, ant_number, alpha, beta, evaporateCoeff, randomCity);
myColony->CreateSolution(max_iter);
myColony->PrintSolution();




/*  Testing random number generator.
srand(time(NULL));
int lessThanTen = 0;
int aroundFifty = 0;
int aroundEighty = 0;
int size = 100000;

 for (int i = 0; i<size; i++)
 {
	int randV = rand() % 100;

    if (randV < 10)
    	lessThanTen++;

    if (randV < 50)
    	aroundFifty++;

    if (randV < 80)
    	aroundEighty++;

 }

 std::cout<<"Less then 10 percent"<<float(lessThanTen)/size<<std::endl;
 std::cout<<"Less then 50 percent"<<float(aroundFifty)/size<<std::endl;
 std::cout<<"Less then 80 percent"<<float(aroundEighty)/size<<std::endl;

*/

}
