/*
 * Implementation of the class Ant
 *
 *  Created on: Mar 18, 2016
 *      Author: kina
 */

#include "Ant.h"

Ant::Ant(float* problem, int problemSize, float evaporateCoeff, float alpha, float beta, unsigned randomCityProbability)
{
   mPheromoneDeposits = problem;
   mProblemSize = problemSize;

   // initialize with "empty decision" values,
   // todo cpp--11 allows to initialize with certain numbers.
   for (int i = 0; i<problemSize; i++)
	   mSolution.push_back(NOT_VISITED);

   mSolutionCost = INFINITE_COST;
   mEvaporateCoeff = evaporateCoeff;
   mAlpha = alpha;
   mBeta = beta;
   mRandomCityChoiceProbability = randomCityProbability;
}

Ant::~Ant()
{
 ;
}

int Ant::DecideNextCity(int currentCity)
{
  //std::cout<<"Ant::DecideNextCity is being run"<<std::endl;
  int decision = START_CITY; // Meaning no decision was made so far. By default we are jumping back to start


  // with mRandomCityChoiceProbability% probability jump to a random unvisited city
//  if ((rand() % 100) < mRandomCityChoiceProbability)



  std::vector<int> possibleCandidates;
	 for (int i = 1; i<mProblemSize; i++) // select possible candidates
	 {
        if (mSolution[i] == NOT_VISITED && mSolution[i] != currentCity)
        	possibleCandidates.push_back(i);
	 }

	 //std::cout<<"Possible Candidate number is:"<<possibleCandidates.size()<<std::endl;

	 // exhausted all the search space.
	 if (possibleCandidates.size() == 0)
		 return decision;

	 int shuffledIndex = rand() % (possibleCandidates.size());
	// std::cout<<"shuffledIndex we want to draw:"<<shuffledIndex<<std::endl;
     decision = possibleCandidates[shuffledIndex]; // randomly draw among the available cities
     std::cout<<"Decision made is:"<<decision<<std::endl;
     // if decision at this point == start_city, then it means we have exhausted all the search
     // and it is time to go back to the starting point.
     mSolutionCost = mSolutionCost + CalcTransitionCost(currentCity, decision);
     //mSolution[currentCity] = decision; // already in the CreateSolution()
     return decision;


/*
     std::cout<<"WARNING: we are skipping the section with stohastic path selection section."<<std::endl;


     float denominator = 0;
     float best_path = INFINITE_COST;
     for (int possibleCity = 1; possibleCity<mProblemSize; possibleCity++)
     {
   	 if (mSolution[possibleCity] == NOT_VISITED)
   	 {
   	    float possible_path = pow(CalcTransitionCost(currentCity,possibleCity), mAlpha) * pow(mPheromoneDeposits[currentCity*10 + possibleCity],mBeta);
   	    denominator = denominator + possible_path;
   	 }

     }

     for (int possibleCity = 1; possibleCity<mProblemSize; possibleCity++)
     {
   	  if (mSolution[possibleCity] == NOT_VISITED)
   	  {
   	     float possible_path = pow(CalcTransitionCost(currentCity,possibleCity), mAlpha) * pow(mPheromoneDeposits[currentCity*10 + possibleCity],mBeta);
   	     if (possible_path/denominator < best_path)
   	     {
   		     best_path = possible_path/denominator;
   		     decision = possibleCity;
   	     }
   	  }
     }


     mSolutionCost = mSolutionCost + CalcTransitionCost(currentCity, decision);
     return decision;


*/

}

std::vector<int> Ant::CreateSolution()
{
	std::cout<<"Ant::CreateSolution is being exectued"<<std::endl;
	// always start from a certain city and eventually return to this city.
	int CurrentCity = START_CITY;
	mSolutionCost = 0;
	// find n-1 partial solutions.
    for (int i = 0; i<mProblemSize; i++)
    {
    	mSolution[CurrentCity] = DecideNextCity(CurrentCity);
    	CurrentCity =mSolution[CurrentCity];
    }

    return mSolution;
}


float Ant::SolutionCost() {return mSolutionCost;}



void Ant::DepositPheromones()
{
   int CurrentCity = START_CITY;
   for (int i = 0; i<mProblemSize; i++) {
	  // no "forgetting" like in Background Substraction with Gaussian.
      // mPheromoneDeposits[CurrentCity*10 + mSolution[CurrentCity]] = mPheromoneDeposits[CurrentCity*10 + mSolution[CurrentCity]] * (1 - mEvaporateCoeff) + mEvaporateCoeff * (CalcTransitionCost(CurrentCity, mSolution[CurrentCity])/SolutionCost());
	  mPheromoneDeposits[CurrentCity*10 + mSolution[CurrentCity]] += CalcTransitionCost(CurrentCity, mSolution[CurrentCity])/SolutionCost();
      CurrentCity = mSolution[CurrentCity];
   }
}



//float Ant::TransitionCost(int city1, int city2) {return mTransitionCosts[city1*10+city2];}

float Ant::CalcTransitionCost(int city1, int city2){return sqrt(pow(abs((city1/10) - (city2/10)),2) + pow(abs(city1 % 10 - city2 % 10),2));}

