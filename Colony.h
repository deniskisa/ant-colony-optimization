/*
 * Colony.h
 *
 *  Created on: Mar 18, 2016
 *      Author: kina
 */

#ifndef COLONY_H_
#define COLONY_H_

#include <vector>

#include "Ant.h"

class Colony
{


public:

Colony(float* problem, int problemSize, int antNumber, float alpha, float beta, float evaporateCoeff, unsigned randomCityProbability);
void CreateSolution(int max_iter);
void PrintSolution();
void EvaporatePheromoneTrails();
void FindBestSolution();

private:

float* mPheromones;
int mProblemSize;
int mAntNumber;
float mAlpha;
float mBeta;
float mEvaporateCoeff;
unsigned mRandomCityChoiceProbability;
std::vector<Ant> mAnts;
std::vector<int> mBestSolution;

};


Colony::Colony(float* problem, int problemSize, int antNumber, float alpha, float beta, float evaporateCoeff, unsigned randomCityProbability)
{
   mPheromones = problem;
   mProblemSize = problemSize;
   mAntNumber = antNumber;
   mAlpha = alpha;
   mBeta = beta;
   mEvaporateCoeff = evaporateCoeff;
   mRandomCityChoiceProbability = randomCityProbability;
}



void Colony::CreateSolution(int max_iter)
{

   for (int i = 0; i<max_iter; i++) {

	   // delete old ants.
	   mAnts.clear();

	   // Create new ants and let each ant find the solution once.
	   for (int j = 0; j<mAntNumber; j++) {
	      mAnts.push_back(Ant(mPheromones, mProblemSize, mEvaporateCoeff, mAlpha, mBeta, mRandomCityChoiceProbability));
	      mAnts[j].CreateSolution();
	   }

	   EvaporatePheromoneTrails();

       // deposit trails in accordance with the quality of each ant's solution.
	   for (int j = 0; j<mAntNumber; j++)
	   	  mAnts[j].DepositPheromones();

	   std::cout<<"Iteration"<<i<<". "; FindBestSolution();
   }

   std::cout<<"Stopped evaluating"<<". "; FindBestSolution();
}


void Colony::PrintSolution() {

	std::cout<<"The trip looks like this:"<<std::endl;

	for (int i = 0; i<mProblemSize; i++)
	{
	   std::cout<<mBestSolution[i]<<" ";
	   if (i % 10 == 0) std::cout<<std::endl;
	}

}

void Colony::EvaporatePheromoneTrails() {

	for (int j = 0; j<mProblemSize*mProblemSize; j++)
 	   mPheromones[j] *= (1 - mEvaporateCoeff);

}

void Colony::FindBestSolution() {

   int best_solution = 0; // first ant by default has the best solution
   for (int i = 1; i<mAntNumber; i++)
      if (mAnts[i].SolutionCost() < mAnts[best_solution].SolutionCost())
         best_solution = i;

   mBestSolution = mAnts[best_solution].mSolution;
   std::cout<<"The best solution costs "<<mAnts[best_solution].SolutionCost()<<std::endl;

}

#endif /* COLONY_H_ */
