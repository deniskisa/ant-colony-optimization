/*
 * Ant.h
 *
 *  Created on: Mar 18, 2016
 *      Author: kina
 */

#ifndef ANT_H_
#define ANT_H_

#include "time.h"
#include "stdlib.h"


#include <math.h>
#include <iostream>
#include <vector>

const unsigned START_CITY = 0;
const int NOT_VISITED = -1;
const unsigned INFINITE_COST = 10000;

class Ant
{


public:

float* mPheromoneDeposits; // HASA link to the TSP problem
int mProblemSize; // size of the symmetric matrix with the TSP problem
std::vector<int> mSolution;
float mSolutionCost;
float mEvaporateCoeff;
float mAlpha;
float mBeta;
unsigned int mRandomCityChoiceProbability;


public:

// constructors/destructors
Ant(float* problem, int problemSize, float evaporateCoeff, float alpha, float beta, unsigned randomCityChoiceProbability);

~Ant();

// Interface
std::vector<int> CreateSolution();
void DepositPheromones();
float SolutionCost();


private:

int DecideNextCity(int currentCity);
float CalcTransitionCost(int city1, int city2);

};


#endif /* ANT_H_ */
